import * as React from 'react';
import {ISimpleLens, IValueLink} from './models';

interface IProps{
    onChange: (lens: ISimpleLens<Object, string>, value: string) => void;
    value: string;
    lens: ISimpleLens<Object, string>;
}

export class TextInput extends React.PureComponent<IProps, void> {

    componentWillReceiveProps(nextProps) {
    }

    handleChange = (event) => this.props.onChange(this.props.lens, event.target.value);

    render () {
        return(
            <div>
                <input type="text" onChange={this.handleChange} value={this.props.value}/>
            </div>
        )
    }
}


interface ISecondTextInputProps {
    onChange: (lens: ISimpleLens<Object, {field1: string, field2: number}>, value: {field1: string, field2: number}) => void;
    value: {field1: string, field2: number};
    lens: ISimpleLens<Object, {field1: string, field2: number}>;
}

interface ISecondTextInputState {
    currentValue: {field1: string, field2: number};
}

/**
 * Пример компонента с value - сложной моделью.
 */
export class ComplexInput1 extends React.PureComponent<ISecondTextInputProps, ISecondTextInputState> {

    state: ISecondTextInputState = {
        currentValue: {
            field1: '',
            field2: 0
        }
    }

    componentWillReceiveProps(nextProps) {
    }

    handleField1Change = (event) => {
        const value = event.target.value;
        this.setState(
            prevState => 
                ({currentValue: {...prevState.currentValue, field1: value}}
            ), () => {
                this.props.onChange(this.props.lens, this.state.currentValue);
            });
    }

    handleField2Change = (event) => {
        const value = event.target.value;
        this.setState(
            prevState => 
                ({currentValue: {...prevState.currentValue, field2: value}}
            ), () => {
                this.props.onChange(this.props.lens, this.state.currentValue);
            });
    }

    render () {
        return(
            <div>
                <input type="text" onChange={this.handleField1Change} value={this.props.value.field1}/>
                <input type="number" onChange={this.handleField2Change} value={this.props.value.field2}/>
            </div>
        )
    }
}

/**
 * onChange подсовывается из компонента Field.
 */
interface IClassicComponentProps {
    value: string;
    onChange: (val: string) => void;
    name: string;
    name2: string;
}

export class ClassicComponent extends React.PureComponent<IClassicComponentProps, void> {

    componentWillReceiveProps(nextProps: IClassicComponentProps) {
    }

    handleChange = (event) => {
        //Какой-то код.
        this.props.onChange(event.target.value);
    }

    render () {
        return(
            <div>
                <span>{this.props.name}</span>
                <input type="text" onChange={this.handleChange} value={this.props.value}/>
            </div>
        )
    }
}


/**
 * onChange подсовывается из компонента Field.
 */
export interface INumberClassicComponentProps {
    value: number;
    onChange: (val: number) => void;
    name: string;
}

export class NumberClassicComponent extends React.PureComponent<INumberClassicComponentProps, void> {

    componentWillReceiveProps(nextProps: IClassicComponentProps) {
        debugger;
    }

    handleChange = (event) => {
        //Какой-то код.
        this.props.onChange(event.target.value);
    }

    render () {
        return(
            <div>
                <span>{this.props.name}</span>
                <input type="text" onChange={this.handleChange} value={this.props.value}/>
            </div>
        )
    }
}


