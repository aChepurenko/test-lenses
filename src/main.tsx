import * as React from 'react';
import * as ReactDom from 'react-dom';

import {TextInput, ComplexInput1, ClassicComponent, NumberClassicComponent} from './textInput';
import {NumberInputComponent} from './numberInput';
import {Field} from './Field';
import {ISimpleLens, onChangeHandler} from './models';
import {curry, curryWithLens, lens1, curryWithLens1} from './helper';

interface ISomethingModel {
    numberField: number,
    numberField2: number,
    numberField3: number,
    stringField: string,
    stringField1: string,
    nestedModel: {
        numField: number;
        secondLevelNestedModel: {
            numField: number;
            stringField: string;
        },
        complexModel: {
            field1: string;
            field2: number;
        }
    }
}

interface IState {
    viewModel: ISomethingModel;
}

interface IProps {
    
}

export class Form extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            viewModel: {
                numberField: 1,
                numberField2: 2,
                numberField3: 3,
                stringField: "Hello222",
                stringField1: "Hello555",
                nestedModel: {
                    numField: 4,
                    secondLevelNestedModel: {
                        numField: 5,
                        stringField: 'Hello333'
                    },
                    complexModel: {
                        field1: 'Hello444',
                        field2: 6
                    }
                }
            }
        }

        /** Единственный хендлер на всю форму.
         * Посылаем в компонент коллбек на изменение, value и линзу.
         * Единственный хендлер на все компоненты.
         */
        let handleModelChange = function <TVal>(lens: ISimpleLens<ISomethingModel, TVal>, value: TVal) {
            const viewModel = lens.set({...this.state.viewModel}, value);
            this.setState({viewModel});
        }

        //this.handleModelChange = curry(handleModelChange.bind(this));

        /**
         * Каррированная функция - обработчик.
         */
        this.handleModelChange = curryWithLens(handleModelChange.bind(this));
    }

    //handleModelChange: <TVal>(lens: ISimpleLens<ISomethingModel, TVal>, value: TVal) => void;

    /**
     * Вместо обычной функции-обработчика с двумя параметрами обработчиком будет служить каррированная функция с дженерик-value,
     * которая выполняется тогда, когда получает оба параметра. Линзу мы подсовываем сразу при создании компонента, а value
     * подсовывается при изменении и функция выполняется.
     */
    handleModelChange: onChangeHandler<ISomethingModel>;

    render () {
        const {viewModel} = this.state;

        return (
            <form>
                <NumberClassicComponent
                    value={viewModel.nestedModel.numField}
                    name="sdfsdf"
                    onChange={this.handleModelChange((model: ISomethingModel) => model.nestedModel.numField)}
                />

                <ClassicComponent
                    value={viewModel.stringField1}
                    name="sdfsdf"
                    name2="sdfsdf"
                    onChange={this.handleModelChange((model: ISomethingModel) => model.stringField1)}
                />

                <hr/>
                <div>
                    <div>
                        <span>NestedModel.Number: </span><span>{viewModel.nestedModel.numField}</span>
                    </div>
                    <div>
                        <span>Model.stringField1: </span><span>{viewModel.stringField1}</span>
                    </div>
                </div>
            </form>
        )
    }
}

ReactDom.render(
    <Form />, 
    document.getElementById('app')
);
