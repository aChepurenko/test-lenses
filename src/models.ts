import {ILens, ILensSetter, ILensGetter, ILensModifier} from 'lenticular-ts';

export interface ISimpleLens<TObj, TValue> {
    
    get: ILensGetter<TObj, TValue>;
    set: ILensSetter<TObj, TValue>;
    modify: ILensModifier<TObj, TValue, TValue>;
}

/**
 * Наша модель!
 * Интерфейс биндинга к данным, принятый в ЦКП (предполагается что все компоненты, связанные с моделями будут раширять
 * этот интерфейс в своих IProps).
 */
export interface IValueLink<T> {
    value: T;
    onChange: (val: T) => void;
}

/**
 * Тип хендлера формы.
 */
export type onChangeHandler<TModel> = <TVal>(path: (model: TModel) => TVal) => (val: TVal) => void;
