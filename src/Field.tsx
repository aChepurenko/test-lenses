import * as React from 'react';
import {ISimpleLens, IValueLink} from './models';
import {lens} from './helper';
import {withoutTypeScriptExtension} from "awesome-typescript-loader/dist/helpers";

interface IFieldProps<TVal> {
    children?: (props: IValueLink<TVal>) => JSX.Element;
    value: TVal;
    selector: ISimpleLens<Object, TVal>;
    onChange: (selector: ISimpleLens<Object, TVal>, value: TVal) => void;
}

/**
 * Компонент для того, что бы не переписывать классические компоненты (onChange/value) и прокидывать новый
 * onChange(value, selector) и value
 */
export class Field<TVal> extends React.Component<IFieldProps<TVal>, void> {

    componentWillReceiveProps (nextProps) {
        debugger;
    }

    handleChangeChildren = (value: TVal) => {
        this.props.onChange(this.props.selector, value);
    }

    render () {
        const {children, value} = this.props;
        return <div>{children({value, onChange: this.handleChangeChildren})}</div>
    }
}

export const withField = <TProps extends IValueLink<TVal>, TModel, TVal>(
        Component: React.ClassType<TProps, any, any>) => {

    class WithFieldComponent extends React.Component<TProps, void> {
        private selector: ISimpleLens<TModel, TVal>;

        componentWillMount() {
            //this.selector = lens(field);
        }

        handleChange = (val: TVal) => {
            debugger;
            this.props.onChange(val);
        }

        render () {
            const props = (Object as any).assign({}, this.props, {onChange: this.handleChange});
            return React.createElement(Component, props);
        }
    }

    return WithFieldComponent;
}

