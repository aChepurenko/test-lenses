import {ISimpleLens, IValueLink} from './models';
import {pathFromExpression, lensFromPath, IPath, prettifyPath} from 'lenticular-ts';

/**
 * Обычная функция каррирования.
 */
export function curry<A, B, R>(f: <A, B>(a: A, b: B) => R): (a: A) => (b: B) => R {
    return a => b => f(a, b);
}


interface ILensesCacheItem {
    key: {
        path: string;
        onChangeHandler: Function
    };
    lens: Function;
}

/**
 * Объект для кеширования линз по path и ссылке на хендлер, которым обрабатывается линза.
 */
const lensesComplexCache: ILensesCacheItem[] = []

/**
 * Объект для кеширования линз по path.
 */
const lensesCache: any = {};

/**
 * Функция для получения линзы.
 * @param expression: 
 *      Функция-геттер для получения пути до нужного поля объекта.
 *      Должна быть обязательно чистой функцией для избежания коллизий при получении линз из кеша.
 *      Кеширование сделано для того, что бы работал React.PureComponent (PureRenderMixin).
 *      Фактически из функции 
 *          function (model) {
 *              return model.someField;
 *          }
 *      берется строчка с return, и на ее основе строится path, поэтому если в функции написать что-либо еще,
 *      то работать попросту не будет. 
 */
export const lens1 = <TObj, TValue> (expression: (model: TObj) => TValue): ISimpleLens<TObj, TValue> => {
    const path = pathFromExpression(expression);
    return lensFromPath(path);
}

/**
 * Обычная функция каррирования, которая по пути и считает линзу в одной из возвращаемых функций (не кеширует линзу).
 */
export function curryWithLens1<TModel, TVal>(onChangeHandler: <TModel, TVal>(path: ISimpleLens<TModel, TVal>, value: TVal) => void): (lens: (model: TModel) => TVal) => (val: TVal) => void {
    return path => value => onChangeHandler(lens1(path), value);
}

/**
 * Функция для получения линзы.
 * @param expression:
 *      Функция-геттер для получения пути до нужного поля объекта.
 *      Должна быть обязательно чистой функцией для избежания коллизий при получении линз из кеша.
 *      Кеширование сделано для того, что бы работал React.PureComponent (PureRenderMixin).
 *      Фактически из функции
 *          function (model) {
 *              return model.someField;
 *          }
 *      берется строчка с return, и на ее основе строится path, поэтому если в функции написать что-либо еще,
 *      то работать попросту не будет.
 */
export const lens = <TModel, TValue> (
    expression: (model: TModel) => TValue,
    onChangeHandler: <TModel, TValue>(path: ISimpleLens<TModel, TValue>, value: TValue) => void): (val: TValue) => ISimpleLens<TModel, TValue> => {
    const path = pathFromExpression(expression);
    const key = prettifyPath(path);

    let result = null;

    lensesComplexCache.forEach(item => {
        if((key === item.key.path) && (onChangeHandler === item.key.onChangeHandler)) {
            result = item.lens;
        }
    });

    if(!result) {
        result = (val: TValue) => onChangeHandler(lensFromPath(path), val);

        lensesComplexCache.push({
            key: {
                path: key,
                onChangeHandler
            },
            lens: result
        });
    }

    return result;
}

export function curryWithLens<TModel, TVal>(onChangeHandler: <TModel, TVal>(path: ISimpleLens<TModel, TVal>, value: TVal) => void): (lens: (model: TModel) => TVal) => (val: TVal) => void {
    return path => lens(path, onChangeHandler);
}

