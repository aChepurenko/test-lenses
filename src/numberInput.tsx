import * as React from 'react';

import {IPath, lensFromPath} from 'lenticular-ts';
import {lens} from './helper';
import {ISimpleLens} from './models';

interface IProps<T> {
    onChange: (lens: ISimpleLens<T, number>, value: number) => void;
    lens: ISimpleLens<T, number>;
    value: number;
}

/**
 * Пример Generic компонента.
 */
export class NumberInputComponent<T> extends React.PureComponent<IProps<T>, void> {

    componentWillReceiveProps(nextProps) {
    }

    handleChange = (event) => this.props.onChange(this.props.lens, event.target.value);

    render () {
        const {value, lens} = this.props;
        return(
            <div>
                <input type="number" onChange={this.handleChange} value={value}/>
            </div>
        )
    }
}
